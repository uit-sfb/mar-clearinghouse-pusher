package no.uit.mar.clearinghouse;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.*;

import static java.lang.System.exit;


public final class Configuration
{
    private static InputStream is;

    private String authURL;
    private String authUsername;
    private String authPassword;

    private String apiURL;
    
    private String dbPath;
    private String dbFileName_MarRef;
    private String dbFileName_MarDB;
    private String dbFileName_MarFun;
    private String dbFileName_SarsCoV2DB;
    private String dbCurrent;
    private List<String> dbECO;
    
    private Integer recsPerPush;
    
    private String mode;

    private String defaultOperation;

    private Integer maxLogs;
    
    private String filePath_analyzeRecordIDs;

    static String errorMessagePrefix = "ERROR detected in 'config.yml'! Check the correctness of: ";

    public static Configuration loadConfig()
    {
        try
        {
            if(Utils.objectHasContents(is))
            {
                is.close();
            }
            is = Files.newInputStream(Paths.get("config.yml"));
            return new Yaml().loadAs(is, Configuration.class);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    

    /*
    public Map<String, String> getTestMapStringString()
    {
        return testMapStringString;
    }

    public void setTestMapStringString(Map<String, String> testMapStringString)
    {
        // Manual key:value check
        this.testMapStringString = (Map<String, String>)Utils.objectValidate(testMapStringString, errorMessagePrefix + "testMapStringString");
        Utils.stringValidate(this.testMapStringString.get("testKey1"), errorMessagePrefix + "testMapStringString testKey1");
        Utils.stringValidate(this.testMapStringString.get("testKey2"), errorMessagePrefix + "testMapStringString testKey2");

        //Check key:value with a loop
        this.testMapStringString = (Map<String, String>)Utils.objectValidate(testMapStringString, errorMessagePrefix + "testMapStringString");
        for(String k : this.testMapStringString.keySet())
        {
            Utils.stringValidate(this.testMapStringString.get(k), errorMessagePrefix + "testMapStringString " + k);
        }

        // For a map with file paths
        Utils.objectValidate(testMapStringString, errorMessagePrefix + "testMapStringString");
        this.testMapStringString = new HashMap<String, String>();
        for(String k : testMapStringString.keySet())
        {
            Utils.stringValidate(testMapStringString.get(k), errorMessagePrefix + "testMapStringString " + k);
            this.testMapStringString.put(k, (new File(testMapStringString.get(k))).getAbsolutePath());
        }
    }
    */
    

    
    public String getAuthURL()
    {
        return authURL;
    }

    public void setAuthURL(String authURL)
    {
        this.authURL = Utils.stringValidate(authURL, errorMessagePrefix + "authURL");
    }

    public String getAuthUsername()
    {
        return authUsername;
    }

    public void setAuthUsername(String authUsername)
    {
        this.authUsername = authUsername;
    }

    public String getAuthPassword()
    {
        return authPassword;
    }

    public void setAuthPassword(String authPassword)
    {
        this.authPassword = authPassword;
    }

    public String getApiURL()
    {
        return apiURL;
    }

    public void setApiURL(String apiURL)
    {
        this.apiURL = Utils.stringValidate(apiURL, errorMessagePrefix + "apiURL");
    }

    public String getDbPath()
    {
        return dbPath;
    }

    public void setDbPath(String dbPath)
    {
        this.dbPath = Utils.stringValidate(dbPath, errorMessagePrefix + "dbPath");
        this.dbPath = new File(this.dbPath).getAbsolutePath();
        if(!new File(dbPath).exists())
        {
            System.out.println("\n" + "ERROR: setDbPath: MAR-database directory path must exist, but it doesn't!\n");
            exit(1);
        }
    }

    public String getDbFileName_MarRef()
    {
        return dbFileName_MarRef;
    }
    
    public void setDbFileName_MarRef(String dbFileName_MarRef)
    {
        this.dbFileName_MarRef = this.setDbFileName_Helper(dbFileName_MarRef, "dbFileName_MarRef");
    }

    public String getDbFileName_MarDB()
    {
        return dbFileName_MarDB;
    }
    
    public void setDbFileName_MarDB(String dbFileName_MarDB)
    {
        this.dbFileName_MarDB = this.setDbFileName_Helper(dbFileName_MarDB, "dbFileName_MarDB");
    }
    
    public String getDbFileName_MarFun()
    {
        return dbFileName_MarFun;
    }

    public void setDbFileName_MarFun(String dbFileName_MarFun)
    {
        this.dbFileName_MarFun = this.setDbFileName_Helper(dbFileName_MarFun, "dbFileName_MarFun");
    }

    public String getDbFileName_SarsCoV2DB()
    {
        return dbFileName_SarsCoV2DB;
    }

    public void setDbFileName_SarsCoV2DB(String dbFileName_SarsCoV2DB)
    {
        this.dbFileName_SarsCoV2DB = this.setDbFileName_Helper(dbFileName_SarsCoV2DB, "dbFileName_SarsCoV2DB");;
    }
    
    private String setDbFileName_Helper(String dbFileName, String dbText)
    {
        String res = Utils.stringValidate(dbFileName, errorMessagePrefix + dbText);
        String fileFound = null;
        List<String> fileNamesList = new ArrayList<>();
        String[] fileNamesArray;
        DirectoryStream dirStream;

        try
        {
            dirStream = Files.newDirectoryStream(FileSystems.getDefault().getPath(this.dbPath));
            dirStream.forEach(p -> fileNamesList.add(((Path)p).getFileName().toString()));
            dirStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            exit(1);
        }
        fileNamesArray = fileNamesList.toArray(new String[0]);
        Arrays.sort(fileNamesArray);

        for (String f : fileNamesArray)
        {
            if(f.startsWith(res) && f.endsWith(".json"))
            {
                fileFound = f;
            }
        }
        if(!Utils.objectHasContents(fileFound))
        {
            Utils.objectInvalidAction("ERROR: set" + dbText.substring(0, 1).toUpperCase() + dbText.substring(dbText.length()) + ": MAR-database with name pattern '" +
                    res + "' doesn't exist in the path '" + this.dbPath + "'!");
        }
        return fileFound;
    }

    public String getDbCurrent()
    {
        return dbCurrent;
    }

    public void setDbCurrent(String dbCurrent)
    {
        this.dbCurrent = Utils.stringValidate(dbCurrent, errorMessagePrefix + "dbCurrent");
        if(!this.dbCurrent.equals("MarRef") && !this.dbCurrent.equals("MarDB") && !this.dbCurrent.equals("MarFun") && !this.dbCurrent.equals("SarsCoV2DB"))
        {
            Utils.objectInvalidAction(errorMessagePrefix + "dbCurrent");
        }
    }

    public List<String> getDbECO()
    {
        return dbECO;
    }

    public void setDbECO(List<String> dbECO)
    {
        this.dbECO = (List<String>)Utils.objectValidate(dbECO, errorMessagePrefix + "setDbECO");
        for(String s : this.dbECO)
        {
            if(s == null || s.isEmpty() || !s.toUpperCase().contains("ECO:"))
            {
                Utils.objectInvalidAction("ERROR: setDbECO: ECO '" +
                        s + "' is empty or doesn't satisfy required syntax ('ECO:XXXXXXX').");
            }
        }
    }
    
    public String getMode()
    {
        return mode;
    }

    public void setMode(String mode)
    {
        if(!mode.equalsIgnoreCase("CL") && !mode.equalsIgnoreCase("RUN-ONCE") && !mode.equalsIgnoreCase("CL-ONCE"))
        {
            Utils.objectInvalidAction("Mode must be either CL or RUN-ONCE or CL-ONCE!");
        }
        this.mode = mode;
    }

    public String getDefaultOperation()
    {
        return defaultOperation;
    }

    public void setDefaultOperation(String defaultOperation)
    {
        this.defaultOperation = Utils.stringValidate(defaultOperation, errorMessagePrefix + "defaultOperation");
    }

    public int getMaxLogs()
    {
        return maxLogs;
    }

    public void setMaxLogs(int maxLogs)
    {
        this.maxLogs = Utils.intValidate(maxLogs, errorMessagePrefix + "maxLogs", true);

    }

    public Integer getRecsPerPush()
    {
        return recsPerPush;
    }

    public void setRecsPerPush(Integer recsPerPush)
    {
        this.recsPerPush = Utils.intValidate(recsPerPush, errorMessagePrefix + "recsPerPush", false, true);
    }

    public String getFilePath_analyzeRecordIDs()
    {
        return filePath_analyzeRecordIDs;
    }

    public void setFilePath_analyzeRecordIDs(String filePath_analyzeRecordIDs)
    {
        this.filePath_analyzeRecordIDs = filePath_analyzeRecordIDs;
    }
}
