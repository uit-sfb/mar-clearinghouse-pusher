# MAR ClearingHouse Pusher

Command-line tool for pushing data from [MarRef](https://mmp2.sfb.uit.no/marref/) to the Contextual Data ClearingHouse (CDCH).

## Background

- [CDCH doc (PDF)](https://wwwdev.ebi.ac.uk/ena/clearinghouse/api)
- [CDCH API (Swagger)](https://wwwdev.ebi.ac.uk/ena/clearinghouse/api/doc)

Initially based on [this work](https://github.com/elixir-europe/BioHackathon/tree/master/interoperability/Data%20clearinghouse%2C%20validation%20and%20curation%20of%20BioSamples_ENA_Breeding%20API%20endpoints_MAR%20databases/marref_api/src/main/scala/org/elixir/marref).

For more information, see the [Wiki pages](https://gitlab.com/uit-sfb/mar-clearinghouse-pusher/-/wikis/home).

## Getting started

TODO: Use SBT and add Docker image
  
## Commands:

- "quit" and "exit".
- "help".
- "db-info":  
    Shows the information about the local MAR database.  
    Write "db-info full" for including the listing of all recIDs in the database.
- "token":  
    Get and print a new token for the ClearingHouse API.
- "exec-ro":  
    Execute the pushing procedure in read-only mode (just print information in the output without actually pushing the data to ClearingHouse).
- "exec-rw-single":   
    Execute the pushing procedure for only one local MAR database record (for testing purposes).
- "exec-full":  
    Execute the pushing procedure for the whole local MAR database record (the main/full pushing procedure).
- "rec-get-online":  
    Print all attributes for the record from the Clearing House.  
    Parameters: record ID (SAM...).
- "attr-get-online":  
    Print the attribute value for the given attribute name for the record from the Clearing House.  
    Parameters: record ID (SAM...), attribute name (for example 'Project Name').
- "cur-suppress":  
    Suppress existing curation in the Clearing House.
    Parameters: curation ID.

Not yet implemented:
- "rec-get-offline":  
    Print all attributes for the record from the local MAR database.  
    Parameters: record ID (SAM...).
- "attr-get-offline":  
    Print the attribute value for the given attribute name for the record from the local MAR database.  
    Parameters: record ID (SAM...), attribute name (for example 'Project Name').
- "exec-custom":  
    Push a single attribute of a single record with the information provided in the parameters.  
    Parameters: record ID (SAM...), attributePre (current name in CH), attributePost (new name), valuePost (new value), providerUrl.

  
  
