package no.uit.mar.clearinghouse;

import jline.console.ConsoleReader;
import jline.console.completer.StringsCompleter;
import org.apache.commons.io.output.TeeOutputStream;
import org.json.JSONObject;
import scala.reflect.Manifest;
import scala.reflect.ManifestFactory$;

import java.io.*;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.exit;



public class MainClass
{

    private static Configuration config = null;

    private String userName;
    private String password;
    
    private String dbCurrentFileName;

    private PrintStream printStream;
    private TeeOutputStream printStreamTee;

    private File logsFolderFile;
    private FileOutputStream logsStream;
    private String logFileName;

    private String tokenString;

    private Stopwatch stopwatch;

    enum Commands
    {
        HELP("help"),
        QUIT("quit"),
        EXIT("exit"),
        TOKEN("token"),
        DB_INFO("db-info"),
        EXEC_RO("exec-ro"),
        EXEC_RW_SINGLE("exec-rw-single"),
        EXEC_FULL("exec-full"),
        EXEC_CUSTOM("exec-custom"),
        REC_GET_OFFLINE("rec-get-offline"),
        REC_GET_ONLINE("rec-get-online"),
        ATTR_GET_OFFLINE("attr-get-offline"),
        ATTR_GET_ONLINE("attr-get-online"),
        CUR_SUPPRESS("cur-suppress"),
        ANALYZE_FILE("analyze-file");
        private final String command;
        Commands(String command)
        {
            this.command = command;
        }
        public String getCommand()
        {
            return this.command;
        }
        public static String[] getCommands()
        {
            Commands[] c = values();
            String[] cs = new String[c.length];
            for (int i = 0; i < c.length; i++)
            {
                cs[i] = c[i].getCommand();
            }
            return cs;
        }
    }
    
    private class RecordData
    {
        String recID;
        String source;
        String attrName;
        String attrValue;
        private RecordData(String recID, String source, String attrName, String attrValue)
        {
            this.recID = recID;
            this.source = source;
            this.attrName = attrName;
            this.attrValue = attrValue;
        }
    }
    
    private class PushInfoData
    {
        int recordsCount;
        int attrsCount;
        Map<String, Integer> recAttrsDataCount;
        private PushInfoData()
        {
            this.recordsCount = 0;
            this.attrsCount = 0;
            recAttrsDataCount = new HashMap<>();
        }
        private PushInfoData(int pushRecordsCount, int pushAttrsCount)
        {
            this.recordsCount = pushRecordsCount;
            this.attrsCount = pushAttrsCount;
            recAttrsDataCount = new HashMap<>();
        }
    }

    private MainClass(String userName, String password)
    {
        Utils.printTimestampedMessage(System.out, "\n", "Starting...", "\n");

        this.userName = userName;
        this.password = password;

        if(this.userName != null && this.password != null)
        {
            this.tokenString = tokenGet();
            if(this.tokenString == null || this.tokenString.isEmpty())
            {
                System.out.println("\nERROR: The received TOKEN is null or empty. \n");
                exit(1);
            }
            else if(this.tokenString.contains("\"status\":401") || this.tokenString.contains("Bad credentials"))
            {
                System.out.println("\nERROR: 401, Bad credentials. \n");
                exit(1);
            }
        }

        String path = MainClass.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        try
        {
            URLDecoder.decode(path, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            exit(1);
        }

        dbCurrentFileName = "";

        initOutStreamAndLogs();
        stopwatch = new Stopwatch();
        System.out.println();
    }

    private void initOutStreamAndLogs()
    {
        File[] existingLogs;
        logsFolderFile = new File ("logs");
        System.out.println("Logs folder: " + logsFolderFile.getAbsolutePath());
        if(!logsFolderFile.exists())
        {
            logsFolderFile.mkdir();
        }
        logFileName = "log_" + (new SimpleDateFormat("yyyyMMdd-HHmmss")).format(Calendar.getInstance().getTime()) + ".txt";
        try
        {
            logsStream = new FileOutputStream(new File(logsFolderFile.getAbsolutePath() + "/" + logFileName));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        printStreamTee = new TeeOutputStream(System.out, logsStream);
        printStream = new PrintStream(printStreamTee);
        System.setOut(printStream);
        if(config.getMaxLogs() >= 0)
        {
            int deletedCount = 0;
            existingLogs = logsFolderFile.listFiles();
            Arrays.sort(existingLogs);
            for(int i = 0; i < existingLogs.length - config.getMaxLogs(); i++)
            {
                try
                {
                    existingLogs[i].delete();
                    deletedCount++;
                }
                catch (SecurityException e)
                {
                    System.out.println("ERROR: not possible to delete file: " + existingLogs[i].getName() + "\n");
                    e.printStackTrace();
                }
            }
            if(deletedCount == 1)
            {
                System.out.println("(Deleted " + deletedCount + " old log)");
            }
            else
            {
                System.out.println("(Deleted " + deletedCount + " old logs)");
            }
        }
        System.out.println();
    }

    public static void main(String[] args)
    {
        MainClass mainClass;
        System.out.println();
        String usr = null, pswd = null;
        final String programName = new java.io.File(MainClass.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath())
                .getName();
        final String helpLaunch =
                "Required arguments or config values:\n" +
                "username=API_Username " +
                "password=API_Password \n";

        config = Configuration.loadConfig();
        
        if(args.length == 0)
        {
            if(Utils.objectHasContents(config.getAuthUsername()) && Utils.objectHasContents(config.getAuthPassword()))
            {
                usr = config.getAuthUsername();
                pswd = config.getAuthPassword();
            }
            else
            {
                System.out.println(helpLaunch);
            }
        }
        else
        {
            Utils.objectInvalidAction("This functionality is not implemented yet.");
            List<String> argsList = Arrays.asList(args);
            for(String arg : argsList)
            {
                if(arg.trim().startsWith("username="))
                {
                    usr = arg.trim().replace("username=", "");
                }
                if(arg.trim().startsWith("password="))
                {
                    pswd = arg.trim().replace("password=", "");
                }
            }
            if(argsList.size() < 3 || !Utils.objectHasContents(usr) || !Utils.objectHasContents(pswd))
            {
                Utils.objectInvalidAction(helpLaunch.trim() + " procedure=PROCEDURE_NAME \n");
                return;
            }
            mainClass = new MainClass(usr, pswd);
            for(String arg : argsList)
            {
                if(arg.trim().startsWith("procedure="))
                {
                    System.out.println("\n" + arg + "\n");
                    if(arg.trim().replace("procedure=", "").startsWith("XXXXX") /*PROCEDURE_NAME*/ )
                    {
                        mainClass.stopwatch.start();
                        // HERE: execute XXXXX procedure here.
                        mainClass.printStream.println("Execution time: " + mainClass.stopwatch.stopGetResultReset() + "\n");
                    }
                    else if(arg.trim().replace("procedure=", "").startsWith("YYYYY") /*PROCEDURE_NAME*/ )
                    {
                        mainClass.stopwatch.start();
                        // HERE: execute YYYYY procedure here.
                        mainClass.printStream.println("Execution time: " + mainClass.stopwatch.stopGetResultReset() + "\n");
                    }
                    else
                    {
                        Utils.objectInvalidAction("\nInvalid procedure name.\n");
                    }
                }
                else if(!arg.trim().startsWith("username=") && !arg.trim().startsWith("password="))
                {
                    Utils.objectInvalidAction("\nInvalid command.\n");
                }
            }
            return;
        }
        
        InputStream inStream = new FileInputStream(FileDescriptor.in);
        ConsoleReader reader;
        try
        {
            reader = new ConsoleReader(programName, inStream, System.out, null);
            reader.setPrompt("> ");
            reader.addCompleter(new StringsCompleter(Arrays.asList(Commands.getCommands())));
            String line;
            PrintWriter out = new PrintWriter(reader.getOutput());
            if(!(Utils.objectHasContents(usr) && Utils.objectHasContents(pswd)))
            {
                while(true)
                {
                    out.println("Enter username:");
                    usr = reader.readLine();
                    if(usr.isEmpty())
                    {
                        out.println("Username can't be empty!");
                        continue;
                    }
                    out.println();
                    out.println("Enter password:");
                    reader.setEchoCharacter(new Character('*'));
                    pswd = reader.readLine();
                    if(pswd.isEmpty())
                    {
                        out.println("Password can't be empty!");
                        continue;
                    }
                    reader.setEchoCharacter(null);
                    try
                    {
                        // HERE: check login here implements Closeable
                        break;
                    }
                    catch (Exception e)
                    {
                        out.println("\nLogin failed, check the correctness of username and password, and try again.\n");
                        break;
                    }
                }
                out.println("\nLogged in successfully.\n");
            }

            mainClass = new MainClass(usr, pswd);

            if(config.getMode().equalsIgnoreCase("CL"))
            {
                while ((line = reader.readLine()) != null)
                {
                    String[] cmd = line.replace("'", "\"").split("\\s+(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    config = Configuration.loadConfig();
                    System.out.println("\n");
                    if(cmd[0].equalsIgnoreCase(Commands.TOKEN.getCommand()) && cmd.length == 1)
                    {
                        mainClass.tokenGet();
                        System.out.println();
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.DB_INFO.getCommand()) && (cmd.length == 1 || (cmd.length == 2 && cmd[1].equals("full"))))
                    {
                        if(cmd.length == 2)
                        {
                            mainClass.dbInfo(true);
                        }
                        else
                        {
                            mainClass.dbInfo(false);
                        }
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.EXEC_RO.getCommand()) && cmd.length == 1)
                    {
                        mainClass.procedure_readOnly();
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.EXEC_RW_SINGLE.getCommand()) && cmd.length == 1)
                    {
                        mainClass.procedure_rw_single();
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.EXEC_FULL.getCommand()) && cmd.length == 1)
                    {
                        mainClass.procedure_full();
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.EXEC_CUSTOM.getCommand()) && cmd.length > 4)
                    {
                        out.println("This functionality is not implemented yet.\n\n");
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.REC_GET_OFFLINE.getCommand()) && cmd.length == 2)
                    {
                        out.println("This functionality is not implemented yet.\n\n");
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.REC_GET_ONLINE.getCommand()) && cmd.length == 2)
                    {
                        mainClass.getOnlineData(cmd[1]);
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.ATTR_GET_OFFLINE.getCommand()) && cmd.length == 3)
                    {
                        out.println("This functionality is not implemented yet.\n\n");
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.ATTR_GET_ONLINE.getCommand()) && cmd.length == 3)
                    {
                        mainClass.getOnlineData(cmd[1], cmd[2].replace("\"", "").replace("'", "").replace(" ", "%20"));
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.CUR_SUPPRESS.getCommand()) && cmd.length == 2)
                    {
                        mainClass.curationSuppress(cmd[1]);
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.ANALYZE_FILE.getCommand()) && cmd.length == 1)
                    {
                        mainClass.analyzeRecordIDsInFile();
                    }
                    else if(cmd[0].equalsIgnoreCase(Commands.HELP.getCommand()) && cmd.length == 1)
                    {
                        mainClass.help();
                    }
                    else if ((cmd[0].equalsIgnoreCase(Commands.EXIT.getCommand()) || cmd[0].equalsIgnoreCase(Commands.QUIT.getCommand())) && cmd.length == 1)
                    {
                        break;
                    }
                    else
                    {
                        out.println("\nInvalid command or incorrect number of command parameters.\n" +
                                "(Number of parameters: " + (cmd.length - 1) + ")\n(Parameters: " + Arrays.toString(Arrays.copyOfRange(cmd, 1, cmd.length)) + ")\n");
                    }
                }
            }
            else if(config.getMode().equalsIgnoreCase("RUN-ONCE"))
            {
                mainClass.stopwatch.start();
                if(config.getDefaultOperation().equals("procedure_ro"))
                {
                    mainClass.procedure_readOnly();
                }
                else if(config.getDefaultOperation().equals("procedure_rw_single"))
                {
                    mainClass.procedure_rw_single();
                }
                else if(config.getDefaultOperation().equals("procedure_full"))
                {
                    mainClass.procedure_full();
                }
                else
                {
                    Utils.objectInvalidAction("Invalid default operation: " + config.getDefaultOperation());
                }
                mainClass.printStream.println("\n" + "Execution time: " + mainClass.stopwatch.stopGetResultReset() + "\n\n");
            }
            else
            {
                Utils.objectInvalidAction("Invalid mode: " + config.getMode());
            }
            
            mainClass.printStream.flush();
            mainClass.printStream.close();
            mainClass.printStreamTee.flush();
            mainClass.printStreamTee.close();
            mainClass.logsStream.flush();
            mainClass.logsStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            exit(1);
        }
    }



    private void help()
    {
        String content;
        String helpStartTest = "[//]: # (HELP text start)\n";
        String helpEndText = "[//]: # (HELP text end)";
        try
        {
            content = new Scanner(new File("README.md")).useDelimiter("\\Z").next();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            System.out.println("\n");
            return;
        }
        content = content.substring(content.indexOf(helpStartTest) + helpStartTest.length(), content.indexOf(helpEndText));
        System.out.println(content + "\n");
    }
    
    private void procedure_readOnly()
    {
        procedure_main(true, false);
    }
    private void procedure_rw_single()
    {
        procedure_main(false, true);
    }
    private void procedure_full()
    {
        procedure_main(false, false);
    }
    private void procedure_main(boolean isReadOnlyMode, boolean isSingle)
    {
        dbInfo(false);
        Utils.printTimestampedMessage(System.out, "\n", "Procedure: STARTED.", "\n\n");

        this.tokenString = tokenGet();
        List<SampleModel> marDatabaseList = getDB(true);
        JSONObject currentOnlineRecData;
        List<Property> bsAttrs;
        List<String> recsSingleRequestIDs = new ArrayList<>();
        List<SampleModel> recsSingleRequestData = new ArrayList<>();
        String requestCommand;
        String output = null;
        String outputSummary = null;
        int countRecs = 0;
        int countAttrs = 0;
        int tokenRecCount = 0;
        String currentRecID;
        List<String> dbUniqueAttrNames;

        System.out.println("\n");

        dbUniqueAttrNames = getUniqueAttrNamesInDB(marDatabaseList, true);
        for(SampleModel marRec : marDatabaseList)
        {
            if(config.getDbCurrent().startsWith("SarsCoV2DB"))
            {
// EVEN THOUGH GENBANK IS A LIST, ALL RECORDS HAVE ONLY ONE ACCESSION
                currentRecID = marRec.genbankAccession().get().accession().head().value();
            }
            else
            {
                currentRecID = marRec.biosampleAccession().get().value();
            }
            bsAttrs = getAttrsInRec(marRec, dbUniqueAttrNames);

            // READ-ONLY: compare and update if online record is different
            if(isReadOnlyMode)
            {
                currentOnlineRecData = getOnlineData(currentRecID);
                for (Property bsAttr : bsAttrs)
                {
                    // If Record doesn't exist online
                    if(!Utils.objectHasContents(currentOnlineRecData))
                    {
                        System.out.println("Record with ID '" + currentRecID + "' doesn't exist online.");
                        break;
                    }
                    // If Record exists online
                    else
                    {
                        if(bsAttrs.get(bsAttrs.size() - 1).value().equals(bsAttr.value()))
                        {
                            System.out.println("Record with ID '" + currentRecID + "' already exists online.");
                        }
                    }
                }
                System.out.println("Record index number in the DB: " + (marDatabaseList.lastIndexOf(marRec) + 1));
            }
            
            // READ-WRITE: If Record doesn't exist online (continue)
            // ToDo: handle 'java.io.IOException: Cannot run program "bash": error=7, Argument list too long' ->
            // ToDo: -> by decrementing the number of simultaneously pushed (temporarily, only for the failing request)
            System.out.print("\nRecord to be pushed: '" + currentRecID + "', number of attributes: " + bsAttrs.size() + "\n");
            recsSingleRequestIDs.add(currentRecID);
            recsSingleRequestData.add(marRec);
            if((recsSingleRequestIDs.size() >= config.getRecsPerPush()) || (countRecs + recsSingleRequestIDs.size() == marDatabaseList.size()) || (isSingle && recsSingleRequestIDs.size() == 1))
            {
                System.out.println("\n\n\n");
                requestCommand = chPushCommandGenerate(isReadOnlyMode, recsSingleRequestIDs, recsSingleRequestData, dbUniqueAttrNames);
                output = chPushRecords(isReadOnlyMode, requestCommand);
                countRecs += recsSingleRequestIDs.size();
                tokenRecCount += recsSingleRequestIDs.size();
                for(int i = 0; i < recsSingleRequestIDs.size(); i++)
                {
                    bsAttrs = getAttrsInRec(recsSingleRequestData.get(i), dbUniqueAttrNames);
                    countAttrs += bsAttrs.size();
                }
                if(tokenRecCount > 100)
                {
                    tokenRecCount = 0;
                    this.tokenString = tokenGet();
                }
                System.out.println("Processed Records Count (in this iteration): " + recsSingleRequestIDs.size());
                System.out.println("\nProcessed Records Total: " + countRecs + " out of " + marDatabaseList.size());
                System.out.println("\nProcessed Attributes Total: " + countAttrs + "\n");
                recsSingleRequestIDs = new ArrayList<>();
                recsSingleRequestData = new ArrayList<>();
                config = Configuration.loadConfig();
            }
            if(Utils.objectHasContents(output))
            {
                if(!Utils.objectHasContents(outputSummary))
                {
                    outputSummary = output.trim() + "\n\n";
                }
                else
                {
                    outputSummary += output.trim() + "\n\n";
                }
                output = null;
            }
            System.out.println();
            if(isSingle)
            {
                break;
            }
        }
        
        if(Utils.objectHasContents(outputSummary))
        {
            System.out.println("\n" + "Output Summary: " + "\n\n" + outputSummary.trim() + "\n\n");
        }

        Utils.printTimestampedMessage(System.out, "\n", "Procedure: FINISHED.", "\n");
        System.out.println("Total Processed Records Total: " + countRecs + " out of " + marDatabaseList.size());
        System.out.println("Total Processed Attributes Total: " + countAttrs);
        Utils.printTimestampedMessage(System.out, "\n", "Procedure: FINISHED.", "\n\n\n");
        dbInfo(false);
        System.out.println();
    }

    private JSONObject getOnlineData(String recID)
    {
        return getOnlineData(recID, null);
    }
    private JSONObject getOnlineData(String recID, String attrName)
    {
        String requestCommand;
        if(Utils.objectHasContents(attrName))
        {
            requestCommand = "curl -X GET \"" + config.getApiURL() + "/curations/" + recID + "?attributeName=" + attrName + "\" -H \"accept: */*\"; ";
        }
        else
        {
            requestCommand = "curl -X GET \"" + config.getApiURL() + "/curations/" + recID + "\" -H \"accept: */*\"; ";
        }
        System.out.println();
        System.out.println(requestCommand);
        String responseString = Utils.localExecutor(requestCommand, false, false);
        if(responseString.contains("\"success\":false") || responseString.contains("\"status\":404") || responseString.contains("{ null: null }"))
        {
            if(Utils.objectHasContents(attrName))
            {
                System.out.println("Failed to retrieve Attribute Data: \n" + responseString + "\n");
            }
            else
            {
                System.out.println("Failed to retrieve Record Data: \n" + responseString + "\n");
            }
            return null;
        }
        else if(responseString.isEmpty())
        {
            System.out.println("Failed to execute the CURL command.\n");
            return null;
        }
        if(Utils.objectHasContents(attrName))
        {
            System.out.println("Successfully retrieved Attribute Data: \n\n" + responseString + "\n");
        }
        else
        {
            System.out.println("Successfully retrieved Record Data: \n\n" + responseString + "\n");
        }
        JSONObject responseObj = new JSONObject(responseString);
        return responseObj;
    }
    
    private String chPushRecords(Boolean isReadOnlyMode, String requestCommand)
    {
        if(isReadOnlyMode)
        {
            System.out.println("Request:\n" + requestCommand + "\n");
            return null;
        }
        else
        {
            System.out.println("Request:\n" + requestCommand + "\n\n");
            String responseString = Utils.localExecutor(requestCommand, false);
            if(responseString.contains("\"success\":false") || responseString.contains("\"status\":404") || responseString.contains("{ null: null }"))
            {
                System.out.println("Failed to push Records Data: \n" + responseString);
                return null;
            }
            else if(responseString.isEmpty())
            {
                System.out.println("Failed to execute the CURL command.\n");
                return null;
            }
            System.out.println("Successfully pushed Records Data: \n" + responseString);
            return responseString;
        }
    }
    
    private String chPushCommandGenerate(Boolean isReadOnlyMode, List<String> bsRecIDs, List<SampleModel> bsRecData, List<String> dbUniqueAttrNames)
    {
        RecordData currentMarData;
        String requestCommand;
        List<Property> bsAttrs;
        int recCounter = 0;
        List<String> ECOs = null;
        String currentRecURL;
        
        if(config.getDbCurrent().startsWith("MarRef") || config.getDbCurrent().startsWith("MarDB") || 
                config.getDbCurrent().startsWith("MarFun") || config.getDbCurrent().startsWith("SarsCoV2DB"))
        {
            ECOs = config.getDbECO();
        }
        else
        {
            System.out.println("\n" + "Check the correctness of 'dbCurrent' in the config!\n");
            exit(1);
        }

        requestCommand =
                "curl -X POST \"" + config.getApiURL() + "/curations\" " +
                        "-H \"accept: */*\" " +
                        "-H \"Authorization: Bearer " + this.tokenString + "\" " +
                        "-H \"Content-Type: application/json\" " +
                        "-d \"{\\\"curations\\\": " +
                            "[ ";
        
        for(int j = 0; j < bsRecIDs.size(); j++)
        {
            bsAttrs = getAttrsInRec(bsRecData.get(j), dbUniqueAttrNames);
            for (int i = 0; i < bsAttrs.size(); i++)
            {
                if(config.getDbCurrent().startsWith("SarsCoV2DB"))
                {
                    currentRecURL = "https://covid19.sfb.uit.no/corona-db/#/records/" + bsRecData.get(j).cdbId().get().value();
                }
                else
                {
                    currentRecURL = bsRecData.get(j).mmpID().get().url().get();
                }
                currentMarData = new RecordData(bsRecIDs.get(j), currentRecURL, bsAttrs.get(i).name(), 
                        bsAttrs.get(i).value().replace("\"", "\\\\\\\"").replace("\n", " ").replace("`", "'"));
                
                requestCommand +=
                                "{ " +
                                    "\\\"recordId\\\": \\\"" + currentMarData.recID + "\\\", " +
                                    "\\\"recordType\\\": \\\"sample\\\", " +
                                    "\\\"attributePre\\\": \\\"" + currentMarData.attrName + "\\\", " +
                                    "\\\"attributePost\\\": \\\"" + currentMarData.attrName + "\\\", " +
                                    "\\\"valuePost\\\": \\\"" + currentMarData.attrValue + "\\\", " +
                                    "\\\"providerName\\\": \\\"" + config.getDbCurrent() + "\\\", " +
                                    "\\\"providerUrl\\\": \\\"" + currentMarData.source + "\\\", " +
                                    "\\\"attributeDelete\\\": \\\"false\\\", " +
                                    "\\\"assertionMethod\\\": \\\"automatic assertion\\\", ";
                                    if(config.getDbCurrent().startsWith("SarsCoV2DB"))
                                    {
                                        requestCommand +=
                                            "\\\"dataType\\\": \\\"covid19\\\", " +
                                            "\\\"dataIdentifier\\\": \\\"" + bsRecData.get(j).cdbId().get().value() + "\\\" ";
                                    }
                                    else
                                    {
                                        requestCommand +=
                                            "\\\"assertionEvidences\\\": " +
                                            "[ ";
                                                for(int k = 0; k < ECOs.size(); k++)
                                                {
                                                    requestCommand +=
                                                        "{ " +
                                                            "\\\"identifier\\\": \\\"" + ECOs.get(k) + "\\\" " +
                                                        "}";
                                                    if(k < ECOs.size() - 1)
                                                    {
                                                        requestCommand += ",";
                                                    }
                                                    requestCommand += " ";
                                                }
            
                                                requestCommand +=
                                            "] ";
                                    }
                                    requestCommand += "}";
                if(!(i == bsAttrs.size() - 1 && recCounter == bsRecIDs.size() - 1))
                {
                    requestCommand += ",";
                }
                requestCommand += " ";
    
                if(isReadOnlyMode)
                {
                    System.out.println("READ-ONLY MODE: to be pushed:\n" + "recordID: " + bsRecIDs.get(j) + ", " + "source: " + currentMarData.source +
                            ", attributeName: " + currentMarData.attrName + ", valueNew: " + currentMarData.attrValue + "\n");
                }
            }
            recCounter++;
        }
        requestCommand +=
                            "] " + 
                        "}\"; ";
        
        return requestCommand;
    }
    
    private String curationSuppress(String curID)
    {
        this.tokenString = tokenGet();
        String request =
                "curl -X PATCH \"" + config.getApiURL() + "/curations/" + curID + "/suppress\" -H \"accept: */*\" -H \"Authorization: Bearer " + this.tokenString + "\"";
        
        System.out.println("Request:\n" + request + "\n\n");
        String responseString = Utils.localExecutor(request, false);
        if(responseString.contains("\"success\":false") || responseString.contains("\"status\":404") || responseString.contains("{ null: null }"))
        {
            System.out.println("Failed to suppress Curation '" + curID + "': \n" + responseString);
            return null;
        }
        else if(responseString.isEmpty())
        {
            System.out.println("Failed to execute the CURL command.\n");
            return null;
        }
        System.out.println("Successfully suppressed Curation: \n" + responseString);
        return responseString;
    }

    private String readDB()
    {
        return readDB(false);
    }
    private String readDB(boolean silent)
    {
        File db;
        if(config.getDbCurrent().startsWith("MarRef"))
        {
            this.dbCurrentFileName = config.getDbFileName_MarRef();
        }
        else if(config.getDbCurrent().startsWith("MarDB"))
        {
            this.dbCurrentFileName = config.getDbFileName_MarDB();
        }
        else if(config.getDbCurrent().startsWith("MarFun"))
        {
            this.dbCurrentFileName = config.getDbFileName_MarFun();
        }
        else if(config.getDbCurrent().startsWith("SarsCoV2DB"))
        {
            this.dbCurrentFileName = config.getDbFileName_SarsCoV2DB();
        }
        else
        {
            System.out.println("\n" + "Check the correctness of 'dbCurrent' in the config!\n");
            exit(1);
        }
        db = new File(config.getDbPath() + "/" + this.dbCurrentFileName);
        
        if(!silent)
        {
            System.out.println("DATABASE INFO: " + this.dbCurrentFileName + ", " +
                    db.length() / 1024f / 1024f + " MB / " +
                    db.length() + " b." + "\n");
        }
        String str = null;
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream(db);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        byte[] data = new byte[(int) db.length()];
        try
        {
            fis.read(data);
            fis.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            str = new String(data, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return str;
    }

    private List<SampleModel> getDB()
    {
        return getDB(false);
    }
    private List<SampleModel> getDB(boolean silent)
    {
        String dbString = readDB(silent);
        Manifest<MarRefModel> marManifest = ManifestFactory$.MODULE$.classType(MarRefModel.class);
        MarRefModel marDatabaseJson = JsonUtils$.MODULE$.parse(dbString, marManifest);
        List<SampleModel> marDatabaseList = scala.collection.JavaConversions.seqAsJavaList(marDatabaseJson.records().record());
        return marDatabaseList;
    }
    
    private List<String> getUniqueAttrNamesInDB(List<SampleModel> marRecs, boolean skipNotDefined)
    {
        List<Property> attrs;
        List<String> resAttrNames = new ArrayList<>();
        Property a;
        for (SampleModel sm : marRecs)
        {
            attrs = new ArrayList(Arrays.asList(scala.collection.JavaConversions.seqAsJavaList(sm.toBioschema().additionalProperty()).toArray()));
            for(Iterator<Property> it = attrs.iterator(); it.hasNext(); )
            {
                a = it.next();
                if(!a.dbType().contains(config.getDbCurrent()) || (skipNotDefined && a.value().equals("NotDefined")))
                {
                    it.remove();
                }
            }
            resAttrNames = getUniqueAttrNamesInRec(attrs, resAttrNames);
        }
        return resAttrNames;
    }

    private List<String> getUniqueAttrNamesInRec(List<Property> attrs)
    {
        return getUniqueAttrNamesInRec(attrs, null);
    }
    private List<String> getUniqueAttrNamesInRec(List<Property> attrs, List<String> merge)
    {
        Set<String> names = new HashSet<>();
        List<String> res;
        for(Iterator<Property> it = attrs.iterator(); it.hasNext(); )
        {
            names.add(it.next().name());
        }
        if(merge != null)
        {
            for(String s : merge)
            {
                names.add(s);
            }
        }
        res = new ArrayList(names);
        return res;
    }

    private List<Property> getAttrsInRec(SampleModel sm, List<String> uniqueAttrNames)
    {
        List<Property> recAttrs;
        List<Property> res = new ArrayList<>();
        
        recAttrs = new ArrayList(Arrays.asList(scala.collection.JavaConversions.seqAsJavaList(sm.toBioschema().additionalProperty()).toArray()));

        for (Property p : recAttrs)
        {
            if(uniqueAttrNames.contains(p.name()) && !p.value().equals("NotDefined"))
            {
                res.add(p);
            }
        }
        
        return res;
    }
    
    
    
    private String tokenGet()
    {
        String authCommand = "curl -u " + this.userName + ":" + this.password + " " + config.getAuthURL();

        String res = Utils.localExecutor(authCommand, false, false).trim();

        Utils.printTimestampedMessage(System.out, "\n", "TOKEN: \n" + res, "\n");
        return res;
    }

    private void dbInfo(boolean inclRecIDs)
    {
        List<SampleModel> marDatabaseList = getDB();
        Set<String> attrNames = new HashSet<>();
        List<String> presentRecIDs = new ArrayList<>();
        List<String> uniqueRecIDs = new ArrayList<>();
        List<String> duplicateRecIDs = new ArrayList<>();
        String currentRecID;
        List<String> attrsInDB = getUniqueAttrNamesInDB(marDatabaseList, true);
        List<String> attrsAllInDB = getUniqueAttrNamesInDB(marDatabaseList, false);
        PushInfoData pushInfo = getDbPushInfo(marDatabaseList, attrsAllInDB);

        for(SampleModel marRec : marDatabaseList)
        {
            if(config.getDbCurrent().startsWith("SarsCoV2DB"))
            {
// EVEN THOUGH GENBANK IS A LIST, ALL RECORDS HAVE ONLY ONE ACCESSION
                currentRecID = marRec.genbankAccession().get().accession().head().value();
            }
            else
            {
                currentRecID = marRec.biosampleAccession().get().value();
            }
            
            if(presentRecIDs.contains(currentRecID))
            {
                duplicateRecIDs.add(currentRecID);
            }
            else
            {
                uniqueRecIDs.add(currentRecID);
            }
            presentRecIDs.add(currentRecID);
        }
        System.out.println("Total number of records in the DB: " + marDatabaseList.size());
        System.out.println("Unique BioSample/GenBank IDs in the DB: " + uniqueRecIDs.size());
        System.out.print("Duplicate BioSample IDs in the DB: " + duplicateRecIDs.size() + " | ");
        for(String d : duplicateRecIDs)
        {
            System.out.print(d + " ");
        }
        System.out.println("\n");
        System.out.println("Number of all record attributes for Clearing House: " + attrsAllInDB.size());
        System.out.println("Maximal possible number of all unique record attributes to be pushed: " + marDatabaseList.size() * attrsAllInDB.size() + "\n");
        System.out.println("Number of present record attributes for Clearing House: " + attrsInDB.size());
        System.out.println("Maximal possible number of present unique record attributes to be pushed: " + marDatabaseList.size() * attrsInDB.size() + "\n");
        System.out.println("Expected total number of pushed records: " + pushInfo.recordsCount);
        System.out.println("Expected total number of pushed attributes: " + pushInfo.attrsCount + "\n");
        System.out.println("List of present record attribute names to be pushed to Clearing House: \n" + attrsInDB);
        if(inclRecIDs)
        {
            System.out.println("\nList of record IDs: \n" + uniqueRecIDs + "\n");
        }
        System.out.println("\n");
    }

    private PushInfoData getDbPushInfo()
    {
        return getDbPushInfo(null, null);
    }
    private PushInfoData getDbPushInfo(List<SampleModel> marDatabaseList)
    {
        return getDbPushInfo(marDatabaseList, null);
    }
    private PushInfoData getDbPushInfo(List<SampleModel> marDatabaseList, List <String> dbUniqueAttrNames)
    {
        PushInfoData pushInfo = new PushInfoData();
        String currentID;
        List<Property> currentRecAttrs;
        List<Property> tempRecAttrs;
        Map<String, SampleModel> processed = new HashMap<>();
        SampleModel tempRec;
        String tempRecID, tempRecURL, marRecURL;
        int tempRecAttrsCount = 0;

        if(marDatabaseList == null)
        {
            marDatabaseList = getDB(true);
        }
        if(dbUniqueAttrNames == null)
        {
            dbUniqueAttrNames = getUniqueAttrNamesInDB(marDatabaseList, false);
        }

        for(SampleModel marRec : marDatabaseList)
        {
            if(config.getDbCurrent().startsWith("SarsCoV2DB"))
            {
// EVEN THOUGH GENBANK IS A LIST, ALL RECORDS HAVE ONLY ONE ACCESSION
                currentID = marRec.genbankAccession().get().accession().head().value();
            }
            else
            {
                currentID = marRec.biosampleAccession().get().value();
            }
            
            currentRecAttrs = getAttrsInRec(marRec, dbUniqueAttrNames);

            if(!processed.keySet().contains(currentID))
            {
                pushInfo.recordsCount++;
                pushInfo.attrsCount += currentRecAttrs.size();
                tempRecAttrsCount += currentRecAttrs.size();
                processed.put(currentID, marRec);
            }
            else
            {
                tempRec = processed.get(currentID);
                if(config.getDbCurrent().startsWith("SarsCoV2DB"))
                {
// EVEN THOUGH GENBANK IS A LIST, ALL RECORDS HAVE ONLY ONE ACCESSION
                    tempRecID = tempRec.genbankAccession().get().accession().head().value();
                    tempRecURL = tempRec.cdbId().get().url().get();
                    marRecURL = marRec.cdbId().get().url().get();
                }
                else
                {
                    tempRecID = tempRec.biosampleAccession().get().value();
                    tempRecURL = tempRec.mmpID().get().url().get();
                    marRecURL = marRec.mmpID().get().url().get();
                }

                if(!(currentID.equals(tempRecID) && marRecURL.equals(tempRecURL)))
                {
                    pushInfo.attrsCount += currentRecAttrs.size();
                    tempRecAttrsCount += currentRecAttrs.size();
                }
                else
                {
                    tempRecAttrs = getAttrsInRec(processed.get(currentID), dbUniqueAttrNames);
                    pushInfo.attrsCount += Math.abs(tempRecAttrs.size() - currentRecAttrs.size());
                    tempRecAttrsCount += Math.abs(tempRecAttrs.size() - currentRecAttrs.size());
                    for(Property a1 : tempRecAttrs)
                    {
                        for(Property a2 : currentRecAttrs)
                        {
                            if(a1.name().equals(a2.name()) && !a1.value().equals(a2.value()))
                            {
                                pushInfo.attrsCount++;
                                tempRecAttrsCount++;
                            }
                        }
                    }
                }
            }

            if(pushInfo.recAttrsDataCount.containsKey(currentID))
            {
                pushInfo.recAttrsDataCount.replace(currentID, pushInfo.recAttrsDataCount.get(currentID) + tempRecAttrsCount);
            }
            else
            {
                pushInfo.recAttrsDataCount.put(currentID, tempRecAttrsCount);
            }
            tempRecAttrsCount = 0;
        }
        
        return pushInfo;
    }
    
    private void analyzeRecordIDsInFile()
    {
        if(!Utils.objectHasContents(config.getFilePath_analyzeRecordIDs()))
        {
            return;
        }
        
        // Read the alanyze file and create a list of record IDs present in the file
        String content;
        List<String> contentSplit;
        List<String> contentStringSpaceSplit;
        Map<String, Integer> contentDataList = new HashMap<>();
        String tempStr;
        String regex = "(?i)SAM(?-i)[a-zA-Z]{1,3}[0-9]{6,}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher;
        
        try
        {
            content = new Scanner(new File(config.getFilePath_analyzeRecordIDs())).useDelimiter("\\Z").next();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return;
        }
        contentSplit = Arrays.asList(content.split("\n"));
        for(String s : contentSplit)
        {
            matcher = pattern.matcher(s);
            if (matcher.find())
            {
                tempStr = matcher.group();
                contentStringSpaceSplit = Arrays.asList(s.split(" "));
                contentDataList.put(tempStr, Integer.parseInt(contentStringSpaceSplit.get(contentStringSpaceSplit.size() - 2)));
            }
        }
        
        // Read the DB and generate the found/missing IDs info
        List<SampleModel> marDatabaseList = getDB(true);
        String currentID;
        List<String> duplicate = new ArrayList<>();
        List<String> found = new ArrayList<>();
        List<String> missing = new ArrayList<>();
        List<String> recAttrsCountDiffIDs = new ArrayList<>();
        PushInfoData pushInfo = getDbPushInfo(marDatabaseList, getUniqueAttrNamesInDB(marDatabaseList, false));
        for(SampleModel marRec : marDatabaseList)
        {
            if(config.getDbCurrent().startsWith("SarsCoV2DB"))
            {
// EVEN THOUGH GENBANK IS A LIST, ALL RECORDS HAVE ONLY ONE ACCESSION
                currentID = marRec.genbankAccession().get().accession().head().value();
            }
            else
            {
                currentID = marRec.biosampleAccession().get().value();
            }
            
            if(contentDataList.keySet().contains(currentID))
            {
                if(found.contains(currentID))
                {
                    duplicate.add(currentID);
                }
                else
                {
                    found.add(currentID);
                }
                
                if(pushInfo.recAttrsDataCount.get(currentID) != contentDataList.get(currentID))
                {
                    recAttrsCountDiffIDs.add(currentID);
                }
            }
            else
            {
                missing.add(currentID);
            }
        }
        System.out.println("Record IDs:");
        System.out.println(
                    "Found in the database: " + marDatabaseList.size() + 
                    " | Found in the file: " + found.size() + 
                    " | Missing in the file: " + missing.size() + 
                    " | Duplicated BioSample IDs in the database: " + duplicate.size() + "\n");
        System.out.println("Missing in the file:\n" + missing.toString() + "\n");
        System.out.println("Duplicate BioSamples IDs in the database:\n" + duplicate.toString() + "\n");
        for(String s : recAttrsCountDiffIDs)
        {
            System.out.println("Number of attributes not as inspected: " + s + " | Expected: " + pushInfo.recAttrsDataCount.get(s) + " | Found: " + contentDataList.get(s));
        }
        System.out.println("\n");
    }

    private void test()
    {
        
    }
    
}
