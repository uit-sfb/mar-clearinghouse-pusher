package no.uit.mar.clearinghouse;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.System.exit;

final class Utils
{
    private Utils() { }

    static String getCurrentDateTimeString()
    {
        return (new SimpleDateFormat("HH:mm:ss, dd.MM.yyyy.")).format(Calendar.getInstance().getTime());
    }

    static void printTimestampedMessage(PrintStream out, String prefix, String text, String postfix)
    {
        out.println(prefix + getCurrentDateTimeString() + ": " + text + postfix);
    }

    static String localExecutor(String commands)
    {
        return localExecutor(commands, true, true);
    }
    static String localExecutor(String commands, boolean log)
    {
        return localExecutor(commands, log, true);
    }
    static String localExecutor(String commands, boolean log, boolean exitLog)
    {
        Process p;
        String output = "";
        try
        {
            p = new ProcessBuilder("bash", "-c", commands).start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null)
            {
                output += line + System.getProperty("line.separator");
                if(log)
                {
                    System.out.println(line);
                }
            }
            p.waitFor();
            int exitCode = p.exitValue();
            if(exitLog)
            {
                System.out.println("\nLOCAL EXECUTOR, EXIT CODE: " + exitCode + "\n");
            }
            p.destroy();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return output;
    }

    static List<String> getFileNamesFromPaths(Collection<String> paths)
    {
        return getFileNamesFromPaths(paths.toArray(new String[paths.size()]));
    }

    static List<String> getFileNamesFromPaths(String[] paths)
    {
        List<String> res = new ArrayList<String>();
        for (String s : paths)
        {
            res.add(getFileNameFromPath(s));
        }
        return res;
    }

    static String getFileNameFromPath(String path)
    {
        return new File(path).getName();
    }

    static boolean objectHasContents(Object object)
    {
        if(object instanceof String)
        {
            return object != null && !((String) object).isEmpty();
        }
        else if(object instanceof Map)
        {
            return object != null && !((Map) object).isEmpty();
        }
        else if(object instanceof List)
        {
            return object != null && !((List) object).isEmpty();
        }
        else
        {
            return object != null;
        }
    }

    static Object objectValidate(Object object, String errorMessage)
    {
        if(!objectHasContents(object))
        {
            objectInvalidAction(errorMessage);
        }
        return object;
    }

    static void objectInvalidAction(String errorMessage)
    {
        System.out.println("\n" + errorMessage + "\n");
        exit(1);
    }

    static String stringValidate(String object, String errorMessage)
    {
        return (String)objectValidate(object, errorMessage);
    }
    
    static Integer intValidate(Integer object, String errorMessage, boolean canBeZero)
    {
        return intValidate(object, errorMessage, canBeZero, false);
    }
    static Integer intValidate(Integer object, String errorMessage, boolean canBeZero, boolean positiveOnly)
    {
        if(canBeZero)
        {
            objectValidate(object, errorMessage);
        }
        if(object.intValue() == 0 || (object.intValue() < 0 && positiveOnly))
        {
            objectInvalidAction(errorMessage);
        }
        return object;
    }

}
