package no.uit.mar.clearinghouse

//import no.uit.mar.clearinghouse.BioSchemaTopLevel
//import no.uit.mar.clearinghouse.JsonUtils

case class AccessionModel(value: String, url: Option[String])
case class InnerDateModel(date: Option[String],
                          yearMonth: Option[String],
                          year: Option[String]) {
  lazy val value = date
    .orElse(yearMonth)
    .orElse(year)
}
case class DateModel(`type`: String,
                     date: Option[String],
                     yearMonth: Option[String],
                     year: Option[String],
                     to: Option[InnerDateModel],
                     from: Option[InnerDateModel]
                    ) {
  lazy val value = date
    .orElse(yearMonth)
    .orElse(year)
    .orElse(to.flatMap{t => from.map{f => s"${f.value.getOrElse("NotDefined")}/${t.value.getOrElse("NotDefined")}"}})
}
case class ValueModel(`type`: Option[String] = None, value: String)
case class StringListModel(item: Seq[String])
case class AccessionListModel(accession: Seq[AccessionModel])
case class LatLongModel(latitude: Float,
                        longitude: Float) {
  lazy val value: String = s"${f"$latitude%1.4f"} N ${f"$longitude%1.4f"} E"
}
case class RrnaModel(rrna5S: Int, rrna16S: Int, rrna23S: Int)
case class TypingModel(typing: String, term: String)



case class SampleModel(annotationProvider: Option[String] = None,
                       annotationDate: Option[DateModel],
                       annotationPipeline: Option[String],
                       annotationMethod: Option[StringListModel],
                       annotationSoftwareRevision: Option[ValueModel],
                       featuresAnnotated: Option[StringListModel],
                       refseqCds: Option[Int],
                       ncbiRefseqAccession: Option[AccessionListModel],
                       //genes: Option[Int],
                       //cds: Option[Int],
                       //pseudoGenes: Option[Int],
                       rrnas: Option[RrnaModel],
                       completeRrnas: Option[RrnaModel],
                       partialRrnas: Option[RrnaModel],
                       trnas: Option[Int],
                       ncrna: Option[Int],
                       //frameshiftedGenes: Option[Int],
                       sequencingCenters: Option[String],
                       seqMeth: Option[StringListModel],
                       sequencingDepth: Option[StringListModel],
                       assemblyAccession: Option[AccessionModel],
                       assembly: Option[StringListModel],
                       assemblyVersion: Option[StringListModel],
                       //gcContent: Option[Int],
                       //contigs: Option[Int],
                       numReplicons: Option[Int],
                       genomeLength: Option[Int],
                       plasmids: Option[Int],
                       hostCommonName: Option[String],
                       hostScientificName: Option[String],
                       hostSex: Option[String],
                       hostHealthStage: Option[String],
                       pathogenicity: Option[String],
                       disease: Option[String],
                       bodySampleSite: Option[String],
                       otherClinical: Option[String],
                       investigationType: Option[String],
                       sampleType: Option[String],
                       isolationSource: Option[String],
                       collectionDate: Option[DateModel],
                       altElev: Option[ValueModel],
                       depth: Option[ValueModel],
                       envSalinity: Option[Int],
                       envTemp: Option[ValueModel],
                       geoLocName: Option[Seq[String]],
                       latLon: Option[LatLongModel],
                       isolationCountry: Option[String],
                       envBiome: Option[AccessionModel],
                       envFeature: Option[AccessionModel],
                       envMaterial: Option[AccessionModel],
                       envPackage: Option[String],
                       isolGrowthCondt: Option[AccessionListModel],
                       refBiomaterial: Option[AccessionListModel],
                       cultureCollection: Option[StringListModel],
                       collectedBy: Option[String],
                       biosampleAccession: Option[AccessionModel],
                       isolationComments: Option[String],
                       projectName: Option[String],
                       geoLocNameGaz: Option[String],
                       geoLocNameGazEnvo: Option[AccessionModel],
                       analysisProjectType: Option[String],
                       fullScientificName: Option[String],
                       organism: Option[String],
                       //taxonLineageNames: Option[StringListModel],
                       //taxonLineageIds: Option[AccessionListModel],
                       ncbiTaxonIdentifier: Option[AccessionModel],
                       strain: Option[StringListModel],
                       kingdom: Option[String],
                       phylum: Option[String],
                       `class`: Option[String],
                       order: Option[String],
                       family: Option[String],
                       genus: Option[String],
                       species: Option[String],
                       gramStain: Option[String],
                       cellShape: Option[String],
                       motility: Option[Boolean],
                       sporulation: Option[Boolean],
                       temperatureRange: Option[String],
                       optimalTemperature: Option[ValueModel],
                       halotolerance: Option[String],
                       oxygenRequirement: Option[String],
                       biovar: Option[String],
                       serovar: Option[String],
                       otherTyping: Option[TypingModel],
                       typeStrain: Option[Boolean],
                       antismashTypes: Option[String],
                       antismashClusters: Option[String],
                       chebiId: Option[AccessionModel],
                       chebiName: Option[String],
                       chemblId: Option[AccessionModel],
                       compoundName: Option[String],
                       uniprotId: Option[AccessionModel],
                       mmpID: Option[AccessionModel],
                       bacdiveId: Option[AccessionModel],
                       curationDate: Option[String],
                       implementationDate: Option[String],
                       bioprojectAccession: Option[AccessionModel],
                       genbankAccession: Option[AccessionListModel],
                       silvaAccessionSSU: Option[AccessionModel],
                       silvaAccessionLSU: Option[AccessionModel],
                       uniprotAccession: Option[AccessionListModel],
                       publicationPmid: Option[AccessionListModel],
                       comments: Option[String],
                       baseID: Option[ValueModel],
                       genomeStatus: Option[String],
                       mmpBiome: Option[String],

                       // COVID DB only
                       assemblyAccessionGenbank: Option[AccessionModel],
                       assemblyAccessionRefseq: Option[AccessionModel],
                       //n50: Option[Int],
                       bodySampleSubSite: Option[String],
                       sampCollectDevice: Option[String],
                       cdbId: Option[AccessionModel],
                       gisaidId: Option[AccessionModel],
                       sraRun: Option[AccessionModel],
                       isolateName: Option[String],
                       submissionDate: Option[DateModel],
                       quality: Option[String],
                       dnaShape: Option[String],
                       baltimoreClass: Option[String],
                       morphology: Option[String],
                       //numSegments: Option[Int],
                       submittedBy: Option[String],
                       assemblyTool: Option[StringListModel]
                      ) {
  lazy val toJson: String = JsonUtils.serialize(this)
  lazy val toBioschema: TopLevel = {
    TopLevel(
      `@context` = "http://schema.org",
      `@type` = Seq("BioChemEntity","Sample"),
      `@id` = mmpID.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined"),
      identifier = Seq(s"mmp:${mmpID.map{_.value}.getOrElse{"NotDefined"}}", s"biosample:${biosampleAccession.map{_.value}.getOrElse("NotDefined")}"),
      name = Seq(fullScientificName.getOrElse("NotDefined")),
      description = comments.getOrElse("NotDefined"),
      url = mmpID.map{_.url.getOrElse("NotDefined")}.getOrElse(cdbId.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined")),
      additionalProperty = Seq(
        Seq(Property(name = "Sequencing Depth",
          value = sequencingDepth.map{_.item.mkString(", ")}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "Sequencing Centers",
          value = sequencingCenters.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        Seq(Property(name = "Comments",
          value = comments.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        Seq(Property(name = "Isolation Comments",
          value = isolationComments.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "Isolation Country",
          value = isolationCountry.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        publicationPmid.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "Publication PMID",
            value = a.value,
            dbType = "MarRef MarDB MarFun SarsCoV2DB"
          )
        },
        Seq(Property(name = "Collection Date",
          value = collectionDate.flatMap{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "Depth",
          value = depth.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        Seq(Property(name = "Latitude and Longitude",
          value = latLon.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "Project Name",
          value = projectName.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "Curation Date",
          value = curationDate.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "MMP ID",
          value = mmpID.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        Seq(Property(name = "Silva Accession SSU",
          value = silvaAccessionSSU.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        Seq(Property(name = "Silva Accession LSU",
          value = silvaAccessionLSU.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        uniprotAccession.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "Uniprot Accession",
            value = a.value,
            dbType = "MarRef MarDB MarFun"
          )
        },
        Seq(Property(name = "ENA Assembly accession identifier",
          value = assemblyAccession.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        Seq(Property(name = "ENA BioProject accession identifier",
          value = bioprojectAccession.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "ENA BioSample accession identifier",
          value = biosampleAccession.map{_.value}.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        genbankAccession.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "ENA GenBank accession identifier",
            value = a.value,
            dbType = "MarRef MarDB MarFun SarsCoV2DB"
          )
        },
        ncbiRefseqAccession.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "NCBI Refseq accession identifier",
            value = a.value,
            dbType = "MarRef MarDB MarFun"
          )
        },
        cultureCollection.toSeq.flatMap{_.item.map{i =>
          Property(name = "Culture Collection(s)",
            value = i,
            dbType = "MarRef MarDB MarFun"
          )
        }},
        Seq(Property(name = "Temperature Range",
          value = temperatureRange.getOrElse("NotDefined"),
          dbType = "MarRef MarDB MarFun"
        )),
        refBiomaterial.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "Reference for Biomaterial",
            value = a.value,
            dbType = "MarRef MarDB MarFun"
          )
        },
        Seq(Property(name = "Geographic Location (GAZ)",
          value = geoLocNameGaz.getOrElse("NotDefined"),
          valueReference = Some(Seq(ValueReference(
            codeValue = geoLocNameGazEnvo.map{_.value}.getOrElse("NotDefined"),
            url = geoLocNameGazEnvo.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined")
          ))),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
          )
        ),
        Seq({
          val ov = envFeature.map{_.value.split('(').head.stripLineEnd}
          Property(name = "Environment Feature",
            value = ov.getOrElse("NotDefined"),
            valueReference = Some(Seq(ValueReference(
              name = ov,
              codeValue = envFeature.flatMap{_.value.split('(').tail.headOption}.flatMap{_.split(')').headOption}.getOrElse("NotDefined"),
              url = envFeature.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined")
            ))),
            dbType = "MarRef MarDB MarFun"
          )}),
        Seq({
          val ov = envMaterial.map{_.value.split('(').head.stripLineEnd}
          Property(name = "Environment Material",
            value = ov.getOrElse("NotDefined"),
            valueReference = Some(Seq(ValueReference(
              name = ov,
              codeValue = envMaterial.flatMap{_.value.split('(').tail.headOption}.flatMap{_.split(')').headOption}.getOrElse("NotDefined"),
              url = envFeature.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined")
            ))),
            dbType = "MarRef MarDB MarFun"
          )}),
        Seq(Property(name = "Organism",
          value = organism.getOrElse("NotDefined"),
          valueReference = Some(Seq(ValueReference(
            name = Some("NCBI Taxon Identifier"),
            codeValue = ncbiTaxonIdentifier.map{i => s"NCBITaxon:$i"}.getOrElse("NotDefined"),
            url = ncbiTaxonIdentifier.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined")
          ))),
          dbType = "MarRef MarDB MarFun SarsCoV2DB"
        )),
        Seq(Property(name = "Full Scientific Name",
          value = fullScientificName.getOrElse("NotDefined"),
          valueReference = Some(Seq(ValueReference(
            name = fullScientificName,
            codeValue = ncbiTaxonIdentifier.map{i => s"NCBITaxon:$i"}.getOrElse("NotDefined"),
            url = ncbiTaxonIdentifier.map{_.url.getOrElse("NotDefined")}.getOrElse("NotDefined")
          ))),
          dbType = "MarRef MarDB MarFun"
        )),
        
        
        // COVID DB only
//        Seq(Property(name = "Genes",
//          value = genes.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
//        Seq(Property(name = "CDS",
//          value = cds.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
//        Seq(Property(name = "Pseudogenes",
//          value = pseudoGenes.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
//        Seq(Property(name = "Frameshift Genes",
//          value = frameshiftedGenes.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
        Seq(Property(name = "Sequencing Method",
          value = seqMeth.map{_.item.mkString(", ")}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Assembly Accession Genbank",
          value = assemblyAccessionGenbank.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Assembly Accession Refseq",
          value = assemblyAccessionRefseq.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Assembly Version",
          value = assemblyVersion.map{_.item.mkString(", ")}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
//        Seq(Property(name = "GC Content",
//          value = gcContent.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
//        Seq(Property(name = "Contigs",
//          value = contigs.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
//        Seq(Property(name = "N50",
//          value = n50.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
        Seq(Property(name = "Genomic Length",
          value = genomeLength.map(_.toString).getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Host Common Names",
          value = hostCommonName.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Host Scientific Name",
          value = hostScientificName.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Host Sex",
          value = hostSex.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Host Health Stage",
          value = hostHealthStage.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        // hostAge,
        Seq(Property(name = "Pathogenicity",
          value = pathogenicity.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Disease",
          value = disease.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Body Sample Site",
          value = bodySampleSite.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Body Sample SubSite",
          value = bodySampleSubSite.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Other Clinical",
          value = otherClinical.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Investigation Type",
          value = investigationType.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Isolation Source",
          value = isolationSource.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Environmental Package",
          value = envPackage.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        isolGrowthCondt.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "Isolation Growth Condition",
            value = a.value,
            dbType = "SarsCoV2DB"
          )
        },
        refBiomaterial.toSeq.flatMap{_.accession}.map{a =>
          Property(name = "Reference Biomaterial",
            value = a.value,
            dbType = "SarsCoV2DB"
          )
        },
        Seq(Property(name = "Collected By",
          value = collectedBy.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Biosample Accession",
          value = biosampleAccession.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Geographic Location (GAZ) Envo",
          value = geoLocNameGazEnvo.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
//        Seq(Property(name = "Taxon Lineage Names",
//          value = taxonLineageNames.map{_.item.mkString(", ")}.getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
//        Seq(Property(name = "NCBI Taxon Identifier",
//          value = ncbiTaxonIdentifier.map{_.value}.getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
        Seq(Property(name = "Kingdom",
          value = kingdom.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Family",
          value = family.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Genus",
          value = genus.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Species",
          value = species.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        // otherTyping,
        Seq(Property(name = "Implementation Date",
          value = implementationDate.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Genome Status",
          value = genomeStatus.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Sample Collect Device",
          value = sampCollectDevice.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Covid DB ID",
          value = cdbId.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "GISAID database accession",
          value = gisaidId.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Sequence Read Archive (SRA)",
          value = sraRun.map{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Isolate Name",
          value = isolateName.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Submission Date",
          value = submissionDate.flatMap{_.value}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Quality",
          value = quality.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "DNA Shape",
          value = dnaShape.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Baltimore Class",
          value = baltimoreClass.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Morphology",
          value = morphology.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
//        Seq(Property(name = "Number Of Segments",
//          value = numSegments.map(_.toString).getOrElse("NotDefined"),
//          dbType = "SarsCoV2DB"
//        )),
        Seq(Property(name = "Submitted By",
          value = submittedBy.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        )),
        Seq(Property(name = "Assembly Tool",
          value = assemblyTool.map{_.item.mkString(", ")}.getOrElse("NotDefined"),
          dbType = "SarsCoV2DB"
        ))
        
      ).flatten
    )
  }
  lazy val toJsonld: String = JsonUtils.serialize(toBioschema)
}

case class RecordsModel(databaseType: String, record: Seq[SampleModel])

case class MarRefModel(records: RecordsModel)
